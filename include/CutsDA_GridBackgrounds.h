#ifndef CutsDA_GridBackgrounds_H
#define CutsDA_GridBackgrounds_H

#include "EventBase.h"
#include "DA_GridBackgroundsEvent.h"

class CutsDA_GridBackgrounds {

public:
    CutsDA_GridBackgrounds(DA_GridBackgroundsEvent* m_evt);
    ~CutsDA_GridBackgrounds();
    bool DA_GridBackgroundsCutsOK(int pulseID);
    bool RandomTrigger();
    bool CutPreTrigger(int pulseID);

private:
    DA_GridBackgroundsEvent* m_evt;
};

#endif
