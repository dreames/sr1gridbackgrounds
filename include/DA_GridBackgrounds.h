#ifndef DA_GridBackgrounds_H
#define DA_GridBackgrounds_H

#include "Analysis.h"

#include "CutsDA_GridBackgrounds.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

#include "DA_GridBackgroundsEvent.h"
#include <TH3I.h>


class SkimSvc;

class DA_GridBackgrounds : public Analysis {

public:
    DA_GridBackgrounds();
    ~DA_GridBackgrounds();

    void Initialize();
    void Execute();
    void Finalize();

protected:
    CutsDA_GridBackgrounds* m_cutsGridBG;
    ConfigSvc* m_conf;
    DA_GridBackgroundsEvent* m_evt;
   
    // variables derived from config settings
    int nTimeBins;
    double firstTriggerTime;
    float windowSize;

    // ROIs
    TH3I* se_xyt;
    TH3I* spe_xyt;
    vector<string> ROI_names;
    vector<int> ROI_starts;
    vector<int> ROI_windowSize;
    int ROI_nBins;
    vector<TH3I*> ROI_se_hists;
    vector<TH3I*> ROI_spe_hists;
};

#endif
