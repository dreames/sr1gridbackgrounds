#ifndef DA_GridBackgroundsEVENT_H
#define DA_GridBackgroundsEVENT_H

#include "EventBase.h"

#include <string>
#include <vector>

#include "rqlibProjectHeaders.h"

class DA_GridBackgroundsEvent : public EventBase {

public:
    DA_GridBackgroundsEvent(EventBase* eventBase);
    virtual ~DA_GridBackgroundsEvent();

    // Event RQs 
    TTreeReaderValue<unsigned long>  triggerTime_s;
    TTreeReaderValue<unsigned long>  triggerTime_ns;
    TTreeReaderValue<int>            triggerType;
    TTreeReaderValue<int>            nPulses;
    TTreeReaderValue<vector<string>> classification;
    TTreeReaderValue<vector<int>>    pulseStartTime_ns;
    TTreeReaderValue<vector<float>>  pulseArea;
    TTreeReaderValue<vector<float>>  s2Xpos_cm;
    TTreeReaderValue<vector<float>>  s2Ypos_cm;
    TTreeReaderValue<vector<float>>  topCentroidX_cm;
    TTreeReaderValue<vector<float>>  topCentroidY_cm;

private:
};

#endif // DA_GridBackgroundsEVENT_H
