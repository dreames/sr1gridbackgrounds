#include "CutsDA_GridBackgrounds.h"
#include "ConfigSvc.h"

CutsDA_GridBackgrounds::CutsDA_GridBackgrounds(DA_GridBackgroundsEvent* DA_GridBackgrounds_Event)
{
    m_evt = DA_GridBackgrounds_Event;
}

CutsDA_GridBackgrounds::~CutsDA_GridBackgrounds()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsDA_GridBackgrounds::DA_GridBackgroundsCutsOK(int pulseID)
{
    // List of common cuts for this analysis into one cut
    return RandomTrigger() && CutPreTrigger(pulseID);
}

// Random triggers only
bool CutsDA_GridBackgrounds::RandomTrigger()
{
    return *m_evt->triggerType == 32;
}

// Cut pulses pre-trigger time
bool CutsDA_GridBackgrounds::CutPreTrigger(int pulseID)
{
    return (*m_evt->pulseStartTime_ns)[pulseID] > 0;
}
