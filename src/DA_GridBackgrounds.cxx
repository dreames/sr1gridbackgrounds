#include "DA_GridBackgrounds.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsDA_GridBackgrounds.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

// Constructor
DA_GridBackgrounds::DA_GridBackgrounds()
    : Analysis()
    , m_evt(new DA_GridBackgroundsEvent(m_event))
{
    m_event->Initialize();

    // Setup logging
    logging::set_program_name("DA_GridBackgrounds Analysis");

    // Setup the analysis specific cuts.
    m_cutsGridBG = new CutsDA_GridBackgrounds(m_evt);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();

}

// Destructor
DA_GridBackgrounds::~DA_GridBackgrounds()
{
    delete m_cutsGridBG;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void DA_GridBackgrounds::Initialize()
{
    INFO("Initializing DA_GridBackgrounds Analysis");
   
    m_cuts->sr1()->Initialize();


    // Run group time boundaries
    firstTriggerTime        = 1643425731;
    double lastTriggerTime  = 1643807910;
    windowSize = lastTriggerTime - firstTriggerTime;
    float timeBinSize = m_conf->configFileVarMap["timeBinSize"]; // seconds
    nTimeBins = windowSize/timeBinSize + 1;

    // Create & add 3D hist
    se_xyt = new TH3I("SE_xyt", "SE_xyt", 500, -100., 100., 500, -100., 100.,
            ROI_nBins, 0, windowSize);
    m_hists->AddHist("SE_xyt", se_xyt);
    spe_xyt = new TH3I("SPE_xyt", "SPE_xyt", 500, -100., 100., 500, -100., 100.,
            ROI_nBins, 0, windowSize);
    m_hists->AddHist("SPE_xyt", spe_xyt);
    
    /*
    // ROIs
    ROI_names       = {"hs0", "str0", "str1", "diff0", "diff1"};
    ROI_starts      = {66800,  40635,  76075,  128750,  170100};
    ROI_windowSize  = {  400,     50,     50,     300,     300};
    ROI_nBins       = 1000; // time bins for 3D hists
    
    // Set up 3D hists for each ROI
    ROI_se_hists.resize(ROI_names.size());
    ROI_spe_hists.resize(ROI_names.size());
    for (int i = 0; i < ROI_names.size(); i++) {
        int startTime = ROI_starts[i];
        int endTime = startTime + ROI_windowSize[i];
        string se_histname = "SE_xyt_" + ROI_names[i];
        TH3I* se_hist = new TH3I(se_histname.c_str(), se_histname.c_str(), 500,
                -100., 100., 500, -100., 100., ROI_nBins, startTime, endTime);
        ROI_se_hists[i] = se_hist;
        m_hists->AddHist(se_histname, se_hist);
        string spe_histname = "SPE_xyt_" + ROI_names[i];
        TH3I* spe_hist = new TH3I(spe_histname.c_str(), spe_histname.c_str(),
                500, -100., 100.,500, -100., 100., ROI_nBins, startTime, endTime);
        ROI_spe_hists[i] = spe_hist;
        m_hists->AddHist(spe_histname, spe_hist);
    }*/
    INFO("Initialization complete");
}

// Execute() - Called once per event.
void DA_GridBackgrounds::Execute()
{
    m_cuts->sr1()->InitializeETrainVetoInEvent();

    double triggerTimeRel_s  = *m_evt->triggerTime_s - firstTriggerTime;
    double triggerTimeRel = (*m_evt->triggerTime_ns)*1.e-9 + triggerTimeRel_s;
    /*
    // determine ROI
    int ROI_idx = -1;
    for (int i = 0; i < ROI_names.size(); i++) {
        int ROI_start = ROI_starts[i];
        int ROI_end = ROI_starts[i] + ROI_windowSize[i];
        if (triggerTimeRel >= ROI_start && triggerTimeRel <= ROI_end) {
            ROI_idx = i;
            break;
        }
    }*/

    // livetime
    m_hists->BookFillHist("livetime", nTimeBins, 0., windowSize, triggerTimeRel);
    /*if (ROI_idx > -1) {
        string histname = "livetime_" + ROI_names[ROI_idx];
        m_hists->BookFillHist(histname, ROI_nBins, ROI_starts[ROI_idx], 
                ROI_starts[ROI_idx] + ROI_windowSize[ROI_idx], triggerTimeRel);
    }*/

    // keep track of SE & SPE times for delta T calculation
    vector<int> SE_startTimes_ns;
    vector<int> SPE_startTimes_ns;

    // plot instantaneous event pulse rate
    float eventWindow = 4.e-3; // seconds
    float pulseRate = (*m_evt->nPulses)/eventWindow;
    m_hists->BookFillHist("LogEventLevelPulseRate", 5000, 0., windowSize, 500, 3.5, 6., triggerTimeRel, TMath::Log10(pulseRate));
    
/*
    //// loop over pulses
    for (int pulseID=0; pulseID<(*m_evt->nPulses); pulseID++) { 
        string classification = (*m_evt->classification)[pulseID];
        
//        if (m_cuts->sr1()->PassesETrainVeto(pulseID, classification)) {

            if (classification == "SPE") {
                SPE_startTimes_ns.push_back((*m_evt->pulseStartTime_ns)[pulseID]);
                float xPos = (*m_evt->topCentroidX_cm)[pulseID];
                float yPos = (*m_evt->topCentroidY_cm)[pulseID];
                m_hists->BookFillHist("SPErate", nTimeBins, 0., windowSize,
                        triggerTimeRel);
                spe_xyt->Fill(xPos, yPos, triggerTimeRel);
               // if (ROI_idx > -1) {
               //     ROI_spe_hists[ROI_idx]->Fill(xPos, yPos, triggerTimeRel);
               // }
            } else if (classification == "SE") {
                SE_startTimes_ns.push_back((*m_evt->pulseStartTime_ns)[pulseID]);
                float pulseArea = (*m_evt->pulseArea)[pulseID];
                float xPos = (*m_evt->s2Xpos_cm)[pulseID];
                float yPos = (*m_evt->s2Ypos_cm)[pulseID];
                m_hists->BookFillHist("SErate", nTimeBins, 0., windowSize,
                        triggerTimeRel);
                se_xyt->Fill(xPos, yPos, triggerTimeRel);
                m_hists->BookFillHist("pulseArea", 1000, 0., 500., pulseArea);
               // if (ROI_idx > -1) {
               //     ROI_se_hists[ROI_idx]->Fill(xPos, yPos, triggerTimeRel);
               // }
            } else if (classification == "S2") {
                float pulseArea = (*m_evt->pulseArea)[pulseID];
                m_hists->BookFillHist("S2rate", nTimeBins, 0., windowSize,
                        triggerTimeRel);
                m_hists->BookFillHist("pulseArea", 1000, 0., 500., pulseArea);
            }
       // }
    }

    // time between SEs & SPEs -- look for photon emission pre-SE
    if (SE_startTimes_ns.size() > 0 && SPE_startTimes_ns.size() > 0){
        int SPE_start_idx = 0;
        for (int i = 0; i < SE_startTimes_ns.size(); i++) {
            int lastSE = 0;
            if (i > 0) { lastSE = SE_startTimes_ns[i-1]; }
            int thisSE = SE_startTimes_ns[i];
            if (thisSE - lastSE >= 10000) { // select for >10us since last SE
                // get first SPE before this SE. 
                // iterate starting index to shorten this loop for subsequent SEs 
                int start_j = SPE_start_idx;
                for (int j = start_j; j < SPE_startTimes_ns.size(); j++) {
                    int delta_t = SPE_startTimes_ns[j] - thisSE;
               
                    // iterate start index to shorten future loops
                    if (delta_t < -5000) { 
                        SPE_start_idx++;
                        continue;
                    }
                    if (delta_t > 0) { break; }
                    m_hists->BookFillHist("SPE_SE_deltaT", nTimeBins, 0.,
                            windowSize, 500., -5000., 0., triggerTimeRel, delta_t);
                }
            } 
        }
    }
    */
    // cleanup for etrain veto
    m_cuts->sr1()->DeleteInactiveProgenitors();
}

// Finalize() - Called once after event loop.
void DA_GridBackgrounds::Finalize()
{
    INFO("Finalizing DA_GridBackgrounds Analysis");
}
