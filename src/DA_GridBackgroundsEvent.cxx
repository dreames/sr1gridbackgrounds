#include "DA_GridBackgroundsEvent.h"

DA_GridBackgroundsEvent::DA_GridBackgroundsEvent(EventBase* base) :
    triggerTime_s(base->m_reader, "eventHeader.triggerTimeStamp_s"),
    triggerTime_ns(base->m_reader, "eventHeader.triggerTimeStamp_ns"),
    triggerType(base->m_reader, "eventHeader.triggerType"),
    nPulses(base->m_reader, "pulsesTPC.nPulses"),
    classification(base->m_reader, "pulsesTPC.classification"),
    pulseStartTime_ns(base->m_reader, "pulsesTPC.pulseStartTime_ns"),
    pulseArea(base->m_reader, "pulsesTPC.pulseArea_phd"),
    s2Xpos_cm(base->m_reader, "pulsesTPC.s2Xposition_cm"),
    s2Ypos_cm(base->m_reader, "pulsesTPC.s2Yposition_cm"),
    topCentroidX_cm(base->m_reader, "pulsesTPC.topCentroidX_cm"),
    topCentroidY_cm(base->m_reader, "pulsesTPC.topCentroidY_cm")
{
}

DA_GridBackgroundsEvent::~DA_GridBackgroundsEvent()
{
}
